CREATE NEW PROJECT STEPS:
- Add New Solution Folders in the Solution file.
- Go to the Solution Folder, add the new folders manually.
- Open a new VStudio instance. Create the new project and point the folder to the newly created folder (in the "src" folder).
- Back to the Solution in previous VS instance, add existing folder and point to the newly created csproj file.

Subcribers and Consumers:
- Subcribers basically are the ones who listen to the domain event and do the next work.
- Consumers are the ones who listen to the messages which are published by other services. (i.e via dbContext.Publish, Publish is MassTransit method).

Package manager console:
- To use PMC, you need to run this: $env:ASPNETCORE_ENVIRONMENT='Developement'.
- Then run the command as usual.