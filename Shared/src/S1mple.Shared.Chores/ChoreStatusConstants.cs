﻿namespace S1mple.Shared.Chores
{
    public static class ChoreStatusConstants
    {
        public static readonly string New = "New";

        public static readonly string InProgress = "In-progress";

        public static readonly string Done = "Done";
    }
}
