﻿using Serilog;

namespace S1mple.Shared.Tools
{
    public static class LoggerInitializer
    {
        public static ILogger Initialize()
        {
            return new LoggerConfiguration()
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss.fff} {Level:u3}] {Message:lj} {Properties}{NewLine}{Exception}")
                .CreateLogger();
        }
    }
}
