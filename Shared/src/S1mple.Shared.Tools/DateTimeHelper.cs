﻿namespace S1mple.Shared.Tools
{
    public static class DateTimeHelper
    {
        public static DateTime GetCurrentDateTimeAtUtc() => DateTime.UtcNow;
    }
}
