﻿using EventFlow.Aggregates;
using Microsoft.Extensions.Logging;
using S1mple.Chores.Domain.Aggregates.Worker.Commands;
using S1mple.Chores.Domain.Aggregates.Worker.Events;

namespace S1mple.Chores.Domain.Aggregates.Worker
{
    public class WorkerAggregate : AggregateRoot<WorkerAggregate, WorkerId>
    {
        private readonly WorkerWriteModel workerState;

        private readonly ILogger<WorkerAggregate> logger;

        public WorkerAggregate(
            WorkerId id,
            ILogger<WorkerAggregate> logger,
            WorkerWriteModel workerState) : base(id)
        {
            this.workerState = workerState;
            this.logger = logger;

            Register(this.workerState);
        }

        public void CreateChore(CreateChoreCommand command)
        {
            Emit(new CreateChoreEvent(command.ChoreName, command.CreatedBy, command.FromDate, command.ToDate));
        }
    }
}
