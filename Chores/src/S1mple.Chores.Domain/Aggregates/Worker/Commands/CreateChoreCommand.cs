﻿using EventFlow.Commands;
using Microsoft.Extensions.Logging;

namespace S1mple.Chores.Domain.Aggregates.Worker.Commands
{
    public class CreateChoreCommand : Command<WorkerAggregate, WorkerId>
    {
        public CreateChoreCommand(WorkerId aggregateId, string choreName, string createdBy, DateTime? fromDate, DateTime? toDate) : base(aggregateId)
        {
            ChoreName = choreName;
            CreatedBy = createdBy;
            FromDate = fromDate;
            ToDate = toDate;
        }

        public string ChoreName { get; }

        public string CreatedBy { get; }

        public DateTime? FromDate { get; }

        public DateTime? ToDate { get; }
    }

    public class CreateChoreCommandHandler : CommandHandler<WorkerAggregate, WorkerId, CreateChoreCommand>
    {
        private readonly ILogger<CreateChoreCommandHandler> logger;

        public CreateChoreCommandHandler(ILogger<CreateChoreCommandHandler> logger)
        {
            this.logger = logger;
        }

        public override Task ExecuteAsync(WorkerAggregate aggregate, CreateChoreCommand command, CancellationToken cancellationToken)
        {
            aggregate.CreateChore(command);

            return Task.CompletedTask;
        }
    }
}
