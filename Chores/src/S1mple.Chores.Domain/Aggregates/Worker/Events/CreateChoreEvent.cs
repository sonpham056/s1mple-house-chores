﻿using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace S1mple.Chores.Domain.Aggregates.Worker.Events
{
    [EventVersion("CreateChoreEvent", 1)]
    public class CreateChoreEvent : AggregateEvent<WorkerAggregate, WorkerId>
    {
        public CreateChoreEvent(string choreName, string createdBy, DateTime? fromDate, DateTime? toDate)
        {
            ChoreName = choreName;
            CreatedBy = createdBy;
            FromDate = fromDate;
            ToDate = toDate;
        }

        public string ChoreName { get; }

        public string CreatedBy { get; }

        public DateTime? FromDate { get; }

        public DateTime? ToDate { get; }
    }
}
