﻿using EventFlow.Core;

namespace S1mple.Chores.Domain.Aggregates.Worker
{
    public class WorkerId : Identity<WorkerId>
    {
        public WorkerId(string value) : base(value)
        {
        }
    }
}
