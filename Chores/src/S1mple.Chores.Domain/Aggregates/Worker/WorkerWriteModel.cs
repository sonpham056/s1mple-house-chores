﻿using EventFlow.Aggregates;

namespace S1mple.Chores.Domain.Aggregates.Worker
{
    public class WorkerWriteModel :
        AggregateState<WorkerAggregate, WorkerId, WorkerWriteModel>
    {
        public string WorkerName { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }
    }
}
