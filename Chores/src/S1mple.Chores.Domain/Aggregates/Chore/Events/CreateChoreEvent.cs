﻿using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace S1mple.Chores.Domain.Aggregates.Chore.Eventss
{
    [EventVersion("CreateChoreEvent", 1)]
    public class CreateChoreEvent : AggregateEvent<ChoreAggregate, ChoreId>
    {
        public CreateChoreEvent(string choreName, string createdBy, DateTime? fromDate, DateTime? toDate)
        {
            ChoreName = choreName;
            CreatedBy = createdBy;
            FromDate = fromDate;
            ToDate = toDate;
        }

        public string ChoreName { get; }

        public string CreatedBy { get; }

        public DateTime? FromDate { get; }

        public DateTime? ToDate { get; }
    }
}
