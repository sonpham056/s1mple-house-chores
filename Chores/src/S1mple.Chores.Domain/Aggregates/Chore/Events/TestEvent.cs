﻿using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace S1mple.Chores.Domain.Aggregates.Chore.Events
{
    [EventVersion("AssignChoreEvent", 1)]
    public class TestEvent : AggregateEvent<ChoreAggregate, ChoreId>
    {
        public TestEvent(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
