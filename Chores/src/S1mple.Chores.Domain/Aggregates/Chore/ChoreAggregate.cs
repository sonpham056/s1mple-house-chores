﻿using EventFlow.Aggregates;
using Microsoft.Extensions.Logging;
using S1mple.Chores.Domain.Aggregates.Chore.Commands;
using S1mple.Chores.Domain.Aggregates.Chore.Events;

namespace S1mple.Chores.Domain.Aggregates.Chore
{
    public class ChoreAggregate : AggregateRoot<ChoreAggregate, ChoreId>
    {
        private readonly ChoreWriteModel aggregateState;

        private readonly ILogger<ChoreAggregate> logger;

        public ChoreAggregate(
            ChoreId id,
            ILogger<ChoreAggregate> logger,
            ChoreWriteModel aggregateState) : base(id)
        {
            this.logger = logger;
            this.aggregateState = aggregateState;

            Register(this.aggregateState);
        }

        public void Test(TestCommand testCommand)
        {
            Emit(new TestEvent(testCommand.Name));
        }

        public void CreateChore(CreateChoreCommand command)
        {
            Emit(new CreateChoreEvent(command.ChoreName, command.CreatedBy, command.FromDate, command.ToDate));
        }
    }
}
