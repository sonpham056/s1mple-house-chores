﻿using EventFlow.Core;

namespace S1mple.Chores.Domain.Aggregates.Chore
{
    public class ChoreId : Identity<ChoreId>
    {
        public ChoreId(string value) : base(value)
        {
        }
    }
}
