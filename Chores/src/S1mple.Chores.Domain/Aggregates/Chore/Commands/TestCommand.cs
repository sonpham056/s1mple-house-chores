﻿using EventFlow.Commands;

namespace S1mple.Chores.Domain.Aggregates.Chore.Commands
{
    public class TestCommand : Command<ChoreAggregate, ChoreId>
    {
        public TestCommand(ChoreId choreId, string name)
            : base(choreId)
        {
            Name = name;
        }

        public string Name { get; }
    }

    public class TestCommandHandler : CommandHandler<ChoreAggregate, ChoreId, TestCommand>
    {
        public override Task ExecuteAsync(ChoreAggregate aggregate, TestCommand command, CancellationToken cancellationToken)
        {
            aggregate.Test(command);

            return Task.CompletedTask;
        }
    }
}
