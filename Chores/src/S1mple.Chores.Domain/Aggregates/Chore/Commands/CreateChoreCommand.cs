﻿using EventFlow.Commands;
using Microsoft.Extensions.Logging;

namespace S1mple.Chores.Domain.Aggregates.Chore.Commands
{
    public class CreateChoreCommand : Command<ChoreAggregate, ChoreId>
    {
        public CreateChoreCommand(ChoreId aggregateId, string choreName, string createdBy, DateTime? fromDate, DateTime? toDate) : base(aggregateId)
        {
            ChoreName = choreName;
            CreatedBy = createdBy;
            FromDate = fromDate;
            ToDate = toDate;
        }

        public string ChoreName { get; }

        public string CreatedBy { get; }

        public DateTime? FromDate { get; }

        public DateTime? ToDate { get; }
    }

    public class CreateChoreCommandHandler : CommandHandler<ChoreAggregate, ChoreId, CreateChoreCommand>
    {
        private readonly ILogger<CreateChoreCommandHandler> logger;

        public CreateChoreCommandHandler(ILogger<CreateChoreCommandHandler> logger)
        {
            this.logger = logger;
        }

        public override Task ExecuteAsync(ChoreAggregate aggregate, CreateChoreCommand command, CancellationToken cancellationToken)
        {
            aggregate.CreateChore(command);

            return Task.CompletedTask;
        }
    }
}
