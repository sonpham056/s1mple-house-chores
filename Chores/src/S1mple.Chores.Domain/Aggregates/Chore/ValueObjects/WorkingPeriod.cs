﻿using EventFlow.ValueObjects;

namespace S1mple.Chores.Domain.Aggregates.Chore.ValueObjects
{
    public class WorkingPeriod : ValueObject
    {
        public WorkingPeriod(DateTime? fromDate, DateTime? toDate)
        {
            FromDate = fromDate;
            ToDate = toDate;
            IsPermanentTask = fromDate.HasValue && toDate.HasValue;
        }

        public DateTime? FromDate { get; private set; }

        public DateTime? ToDate { get; private set; }

        public bool IsPermanentTask { get; private set; }
    }
}
