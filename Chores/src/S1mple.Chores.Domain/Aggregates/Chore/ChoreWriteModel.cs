﻿using EventFlow.Aggregates;
using Microsoft.Extensions.Logging;
using S1mple.Chores.Domain.Aggregates.Chore.Events;
using S1mple.Chores.Domain.Aggregates.Chore.ValueObjects;
using S1mple.Chores.Domain.Aggregates.Worker;
using S1mple.Chores.Domain.Aggregates.Worker.Events;
using S1mple.Shared.Chores;
using S1mple.Shared.Tools;

namespace S1mple.Chores.Domain.Aggregates.Chore
{
    public class ChoreWriteModel :
        AggregateState<ChoreAggregate, ChoreId, ChoreWriteModel>
    {
        private readonly ILogger<ChoreWriteModel> logger;

        public ChoreWriteModel()
        {
        }

        public ChoreWriteModel(ILogger<ChoreWriteModel> logger)
        {
            this.logger = logger;
        }

        public string ChoreName { get; private set; }

        public string ChoreStatus { get; private set; }

        public string CreatedBy { get; private set; }

        public WorkerId[] Workers { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        public WorkingPeriod WorkingPeriod { get; private set; }

        public void Apply(TestEvent @event)
        {
            logger.LogInformation("I'm here {Name}", @event.Name);
        }

        public void Apply(CreateChoreEvent @event)
        {
            ChoreName = @event.ChoreName;
            ChoreStatus = ChoreStatusConstants.New;
            CreatedBy = @event.CreatedBy;
            CreatedAt = DateTimeHelper.GetCurrentDateTimeAtUtc();
            UpdatedAt = CreatedAt;
            WorkingPeriod = new WorkingPeriod(@event.FromDate, @event.ToDate);
        }
    }
}
