﻿using EventFlow.Aggregates;
using EventFlow.ReadStores;
using S1mple.Chores.Domain.Aggregates.Worker.Events;
using S1mple.Shared.Chores;
using S1mple.Shared.Tools;

namespace S1mple.Chores.Domain.Aggregates.Chore.ReadModels
{
    public class ChoreReadModel :
        IReadModel,
        IAmReadModelFor<ChoreAggregate, ChoreId, CreateChoreEvent>
    {
        public string AggregateId { get; set; } = string.Empty;

        public string ChoreName { get; private set; }

        public string ChoreStatus { get; private set; }

        public string CreatedBy { get; private set; }

        public DateTime CreatedAt { get; private set; }

        public DateTime UpdatedAt { get; private set; }

        public DateTime? FromDate { get; private set; }

        public DateTime? ToDate { get; private set; }

        public bool IsPermanentTask { get; private set; }

        public void Apply(IReadModelContext context, IDomainEvent<ChoreAggregate, ChoreId, CreateChoreEvent> domainEvent)
        {
            ChoreName = domainEvent.AggregateEvent.ChoreName;
            FromDate = domainEvent.AggregateEvent.FromDate;
            ToDate = domainEvent.AggregateEvent.ToDate;
            ChoreStatus = ChoreStatusConstants.New;
            CreatedAt = DateTimeHelper.GetCurrentDateTimeAtUtc();
            UpdatedAt = CreatedAt;
            CreatedBy = domainEvent.AggregateEvent.CreatedBy;
        }
    }
}
