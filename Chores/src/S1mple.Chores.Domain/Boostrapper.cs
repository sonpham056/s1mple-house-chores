﻿using Autofac;

namespace S1mple.Chores.Domain
{
    public class Boostrapper : Module
    {
        public static System.Reflection.Assembly Assembly { get; } = typeof(Boostrapper).Assembly;

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly)
                .Where(t => t.FullName != null && t.FullName.StartsWith("S1mple.Chores.Domain.Services", StringComparison.Ordinal))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(Assembly)
                .Where(t => t.IsPublic)
                .AsSelf();
        }
    }
}
