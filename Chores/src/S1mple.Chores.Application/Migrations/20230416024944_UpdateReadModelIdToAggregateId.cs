﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace S1mple.Chores.Application.Migrations
{
    public partial class UpdateReadModelIdToAggregateId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ChoreReadModels",
                newName: "AggregateId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AggregateId",
                table: "ChoreReadModels",
                newName: "Id");
        }
    }
}
