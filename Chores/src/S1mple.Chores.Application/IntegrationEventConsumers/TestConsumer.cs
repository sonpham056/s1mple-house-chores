﻿using EventFlow;
using MassTransit;
using S1mple.Chores.Domain.Aggregates.Chore;
using S1mple.Chores.Domain.Aggregates.Chore.Commands;

namespace S1mple.Chores.Application.IntegrationEventConsumers
{
    public class TestConsumer :
        IConsumer<IntegrationEvents.TestEvent>

    {
        private readonly ICommandBus commandBus;

        public TestConsumer(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        public async Task Consume(ConsumeContext<IntegrationEvents.TestEvent> context)
        {
            var command = new TestCommand(ChoreId.New, context.Message.Name);
            await commandBus.PublishAsync(command, context.CancellationToken);
        }
    }
}
