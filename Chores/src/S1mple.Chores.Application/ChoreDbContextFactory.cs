﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using S1mple.Chores.Application.Data;

namespace S1mple.Chores.Application
{
    // This class is for the Package manager console to deals with migration.
    // Please refer to this link: https://learn.microsoft.com/en-us/ef/core/cli/dbcontext-creation?tabs=dotnet-core-cli
    public class ChoreDbContextFactory : IDesignTimeDbContextFactory<ChoreDbContext>
    {
        public ChoreDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ChoreDbContext>();
            var config = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                            .AddEnvironmentVariables()
                            .Build() as IConfiguration;

            optionsBuilder.UseNpgsql(config.GetConnectionString("DefaultConnection"),
                    providerOptions => providerOptions.EnableRetryOnFailure());

            return new ChoreDbContext(optionsBuilder.Options);
        }
    }
}
