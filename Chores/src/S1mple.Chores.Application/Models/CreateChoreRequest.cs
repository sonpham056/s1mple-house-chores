﻿namespace S1mple.Chores.Application.Models
{
    public class CreateChoreRequest
    {
        public string ChoreName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
