//using Autofac;
//using Autofac.Extensions.DependencyInjection;
//using EventFlow;
//using EventFlow.AspNetCore.Extensions;
//using EventFlow.Autofac.Extensions;
//using EventFlow.DependencyInjection.Extensions;
//using EventFlow.EntityFramework;
//using EventFlow.EntityFramework.Extensions;
//using EventFlow.Extensions;
//using EventFlow.PostgreSql.Connections;
//using EventFlow.PostgreSql.Extensions;
//using MassTransit;
//using Microsoft.EntityFrameworkCore;
//using S1mple.Chores.Application.Data;
//using Serilog;
//using System.Reflection;

//namespace S1mple.Chores.Application
//{
//    public class Program_Old
//    {
//        private static string ApplicationName = "Chores";

//        public static async Task Main(string[] args)
//        {
//            try
//            {
//                Log.Logger = new LoggerConfiguration()
//                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss.fff} {Level:u3}] {Message:lj} {Properties}{NewLine}{Exception}")
//                .CreateLogger();

//                Log.Information("Initializing {ApplicationName}...", ApplicationName);
//                var host = CreateHostBuilder(args).Build();

//                InitializeDatabase(host);

//                await host.RunAsync();
//            }
//            catch (Exception ex)
//            {
//                Log.Error("Error occured while running the app: {ex}", ex.StackTrace);
//                throw;
//            }
//        }

//        private static IHostBuilder CreateHostBuilder(string[] args)
//        {
//            return Host.CreateDefaultBuilder(args)
//                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
//                .ConfigureContainer<ContainerBuilder>(ConfigureEventFlowServices)
//                .ConfigureServices((hostContext, services) =>
//                {
//                    services.AddDbContext<ChoreDbContext>((sp, options) =>
//                    {
//                        var test = hostContext.Configuration.GetConnectionString("DefaultConnection");
//                        options.UseNpgsql(
//                            "Username=s1mple;Password=admin;Server=localhost;Port=5432;Database=TestEventFlow;",
//                            providerOptions => providerOptions.EnableRetryOnFailure());
//                    });

//                    services.AddScoped<IDbContextProvider<ChoreDbContext>, SqlDbContextProvider>();

//                    services.AddEventFlow(ef =>
//                    {
//                        ef.AddDefaults(typeof(Program).Assembly);
//                    });

//                    services.AddMassTransit(x =>
//                    {
//                        x.SetKebabCaseEndpointNameFormatter();

//                        // By default, sagas are in-memory, but should be changed to a durable
//                        // saga repository.
//                        x.SetInMemorySagaRepositoryProvider();

//                        var entryAssembly = Assembly.GetEntryAssembly();

//                        x.AddConsumers(entryAssembly);
//                        x.AddSagaStateMachines(entryAssembly);
//                        x.AddSagas(entryAssembly);
//                        x.AddActivities(entryAssembly);

//                        x.UsingInMemory((context, cfg) =>
//                        {
//                            cfg.ConfigureEndpoints(context);
//                        });
//                    });

//                    services.AddMvcCore().AddApiExplorer();

//                    services.AddSwaggerGen();

//                })
//                .ConfigureWebHostDefaults(config =>
//                {
//                    config.Configure(app =>
//                    {
//                        app.UseRouting();
//                        app.UseSwagger();
//                        app.UseSwaggerUI();
//                    });
//                });
//                //.UseSerilog();
//        }

//        private static void ConfigureEventFlowServices(ContainerBuilder containerBuilder)
//        {
//            var configuration = CreateConfigurationBuilder().Build();

//            EventFlowOptions
//               .New
//               .UseAutofacContainerBuilder(containerBuilder)

//               .ConfigurePostgreSql(PostgreSqlConfiguration.New
//                    .SetConnectionString("Username=s1mple;Password=admin;Server=localhost;Port=5432;Database=TestEventFlow;"))
//               .UsePostgreSqlEventStore()

//               .ConfigureEntityFramework(EntityFrameworkConfiguration.New)
//               //.UseEntityFrameworkReadModel<ChoreReadModel, ChoreDbContext>()

//               .AddEvents(typeof(Program).Assembly)
//               .AddCommands(typeof(Program).Assembly)
//               //.AddQueryHandlers(typeof(Program).Assembly)
//               //.AddSubscribers(typeof(Program).Assembly)

//               // Currently this project doesn't need asp.net core but it will in the future.
//               // Please remove this comment if you need the help of asp.net core
//               // Currently, this line help eventflow use the global config of serilog.
//               .AddAspNetCore(x => x
//                    .RunBootstrapperOnHostStartup()
//                    .UseLogging()
//                    .UseModelBinding()
//                    .AddUserClaimsMetadata());
//        }

//        private static IConfigurationBuilder CreateConfigurationBuilder()
//        {
//            return new ConfigurationBuilder()
//                            .SetBasePath(System.IO.Directory.GetCurrentDirectory())
//                            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
//                            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "Production"}.json", optional: true)
//                            .AddEnvironmentVariables();
//        }

//        private static IHost InitializeDatabase(IHost host)
//        {
//            using (var scope = host.Services.CreateScope())
//            {
//                // Another way to create the 'eventflow' table is this:
//                // var postgreMigrator = scope.ServiceProvider.GetRequiredService<IPostgreSqlDatabaseMigrator>();
//                // EventFlowEventStoresPostgreSql.MigrateDatabase(postgreMigrator);
//                // But put them in a DbInitializer class is a better way.
//                // ActivatorUtitlities can be read in this link: https://onthedrift.com/posts/activator-utilities/
//                var dbInitializer = ActivatorUtilities.CreateInstance<DbInitializer>(scope.ServiceProvider);

//                dbInitializer.Initialize(applyMigration: true);
//            }

//            return host;
//        }
//    }
//}
////var app = builder.Build();

//////app.MapGet("/", () => "Hello World!");

////app.Run();