﻿using EventFlow;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using S1mple.Chores.Application.Models;
using S1mple.Chores.Domain.Aggregates.Chore;
using S1mple.Chores.Domain.Aggregates.Chore.Commands;

namespace S1mple.Chores.Application.Controllers
{
    [Route("chores")]
    public class ChoreController : Controller
    {
        private readonly IBus bus;

        private readonly ICommandBus commandBus;

        public ChoreController(IBus bus, ICommandBus commandBus)
        {
            this.bus = bus;
            this.commandBus = commandBus;
        }

        [HttpPost("test")]
        public async Task<IActionResult> Test(TestModel model)
        {
            await bus.Publish<IntegrationEvents.TestEvent>(new { model.Name });

            return Ok();
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateChore(CreateChoreRequest request, CancellationToken cancellationToken)
        {
            var command = new CreateChoreCommand(
                ChoreId.New,
                request.ChoreName,
                request.CreatedBy,
                request.FromDate,
                request.ToDate);

            await command.PublishAsync(commandBus, cancellationToken);

            return Ok();
        }
    }
}
