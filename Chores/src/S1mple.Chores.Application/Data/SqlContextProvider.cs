﻿using EventFlow.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace S1mple.Chores.Application.Data
{
    public class SqlDbContextProvider : IDbContextProvider<ChoreDbContext>
    {
        private readonly DbContextOptions<ChoreDbContext> dbContextOptions;

        public SqlDbContextProvider(IConfiguration configuration)
        {
            var dbContextBuilder = new DbContextOptionsBuilder<ChoreDbContext>();

            dbContextBuilder.UseNpgsql(
                configuration.GetConnectionString("DefaultConnection"),
                providerOptions => providerOptions.EnableRetryOnFailure());

            dbContextOptions = dbContextBuilder.Options;
        }

        public ChoreDbContext CreateContext()
        {
            return new ChoreDbContext(dbContextOptions);
        }
    }
}
