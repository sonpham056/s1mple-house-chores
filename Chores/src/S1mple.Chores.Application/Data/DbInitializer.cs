﻿using EventFlow.PostgreSql.EventStores;
using EventFlow.PostgreSql;
using Microsoft.EntityFrameworkCore;

namespace S1mple.Chores.Application.Data
{
    public class DbInitializer
    {
        private readonly IPostgreSqlDatabaseMigrator sqlDatabaseMigrator;

        private readonly ChoreDbContext dbContext;

        private readonly ILogger<DbInitializer> logger;

        public DbInitializer(IPostgreSqlDatabaseMigrator sqlDatabaseMigrator, ChoreDbContext dbContext, ILogger<DbInitializer> logger)
        {
            this.sqlDatabaseMigrator = sqlDatabaseMigrator;
            this.dbContext = dbContext;
            this.logger = logger;
        }

        public void Initialize(bool applyMigration)
        {
            try
            {
                if (applyMigration)
                {
                    logger.LogInformation("Applying pending migrations...");
                    dbContext.Database.Migrate();
                }
                else
                {
                    logger.LogInformation("Ensuring database is created without running migrations...");
                    dbContext.Database.EnsureCreated();
                }

                EventFlowEventStoresPostgreSql.MigrateDatabase(this.sqlDatabaseMigrator);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, "Database Initialization error!");
                throw;
            }
        }

    }
}
