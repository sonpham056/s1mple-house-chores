﻿using Microsoft.EntityFrameworkCore;
using S1mple.Chores.Domain.Aggregates.Chore.ReadModels;

namespace S1mple.Chores.Application.Data
{
    public class ChoreDbContext : DbContext
    {
        public ChoreDbContext(DbContextOptions<ChoreDbContext> options)
            : base(options)
        {
        }

        public DbSet<ChoreReadModel> ChoreReadModels => Set<ChoreReadModel>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChoreReadModel>()
                .HasKey(k => k.AggregateId);
        }
    }
}