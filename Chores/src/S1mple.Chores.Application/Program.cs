using Autofac;
using Autofac.Extensions.DependencyInjection;
using EventFlow;
using EventFlow.AspNetCore.Extensions;
using EventFlow.Autofac.Extensions;
using EventFlow.DependencyInjection.Extensions;
using EventFlow.EntityFramework;
using EventFlow.EntityFramework.Extensions;
using EventFlow.Extensions;
using EventFlow.PostgreSql.Connections;
using EventFlow.PostgreSql.Extensions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S1mple.Chores.Application.Data;
using S1mple.Chores.Domain;
using S1mple.Chores.Domain.Aggregates.Chore.ReadModels;
using S1mple.Shared.Tools;
using Serilog;
using System.Reflection;
[assembly: ApiController]

namespace S1mple.Chores.Application
{
    public class Program
    {
        private static readonly string ApplicationName = "Chores";

        private static IConfiguration configuration;

        public static void Main(string[] args)
        {
            Log.Logger = LoggerInitializer.Initialize();
            Log.Information("Initializing {ApplicationName}...", ApplicationName);

            var builder = WebApplication.CreateBuilder(args);

            configuration = builder.Configuration;

            ConfigureServices(builder);

            var app = builder.Build();

            InitializeDatabase(app);

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.MapControllers();

            app.UseSwagger();
            app.UseSwaggerUI();

            app.Run();
        }

        private static void ConfigureServices(WebApplicationBuilder builder)
        {
            builder.Host.UseSerilog();

            ConfigureAutofac(builder);

            // Add services to the container.
            builder.Services.AddControllers();

            ConfigureDatabase(builder);

            AddEventFlowConfigurations(builder);

            AddMassTransitConfiguration(builder);

            builder.Services.AddSwaggerGen();
        }

        private static void AddMassTransitConfiguration(WebApplicationBuilder builder)
        {
            builder.Services.AddMassTransit(x =>
            {
                x.SetKebabCaseEndpointNameFormatter();

                // By default, sagas are in-memory, but should be changed to a durable
                // saga repository.
                x.SetInMemorySagaRepositoryProvider();

                var entryAssembly = Assembly.GetEntryAssembly();

                x.AddConsumers(entryAssembly);
                x.AddSagaStateMachines(entryAssembly);
                x.AddSagas(entryAssembly);
                x.AddActivities(entryAssembly);

                x.UsingInMemory((context, cfg) =>
                {
                    cfg.ConfigureEndpoints(context);
                });
            });
        }

        private static void AddEventFlowConfigurations(WebApplicationBuilder builder)
        {
            builder.Services.AddEventFlow(ef =>
            {
                ef.AddDefaults(typeof(Program).Assembly);
            });
        }

        private static void ConfigureDatabase(WebApplicationBuilder builder)
        {
            builder.Services.AddDbContext<ChoreDbContext>((sp, options) =>
            {
                options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"),
                    providerOptions => providerOptions.EnableRetryOnFailure());
            });

            builder.Services.AddScoped<IDbContextProvider<ChoreDbContext>, SqlDbContextProvider>();
        }

        private static void ConfigureAutofac(WebApplicationBuilder builder)
        {
            // Use autofac service provider
            builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory(builder =>
            {
                builder.RegisterAssemblyModules(Boostrapper.Assembly);
            }));
            builder.Host.ConfigureContainer<ContainerBuilder>(ConfigureEventFlowServicesToAutofacContainer);
        }

        private static void ConfigureEventFlowServicesToAutofacContainer(ContainerBuilder containerBuilder)
        {
            EventFlowOptions
               .New
               .UseAutofacContainerBuilder(containerBuilder)

               .ConfigurePostgreSql(PostgreSqlConfiguration.New
                    .SetConnectionString(configuration.GetConnectionString("DefaultConnection")))
               .UsePostgreSqlEventStore()

               .ConfigureEntityFramework(EntityFrameworkConfiguration.New)
               .UseEntityFrameworkReadModel<ChoreReadModel, ChoreDbContext>()

               .AddDefaults(Boostrapper.Assembly)
               .AddQueryHandlers(typeof(Program).Assembly)
               .AddSubscribers(typeof(Program).Assembly)

               .AddAspNetCore(x => x
                    .RunBootstrapperOnHostStartup()
                    .UseLogging()
                    .UseModelBinding()
                    .AddUserClaimsMetadata());
        }

        private static void InitializeDatabase(WebApplication webBuilder)
        {
            using (var scope = webBuilder.Services.CreateScope())
            {
                // Another way to create the 'eventflow' table is this:
                // var postgreMigrator = scope.ServiceProvider.GetRequiredService<IPostgreSqlDatabaseMigrator>();
                // EventFlowEventStoresPostgreSql.MigrateDatabase(postgreMigrator);
                // But put them in a DbInitializer class is a better way.
                // ActivatorUtitlities can be read in this link: https://onthedrift.com/posts/activator-utilities/
                var dbInitializer = ActivatorUtilities.CreateInstance<DbInitializer>(scope.ServiceProvider);

                dbInitializer.Initialize(applyMigration: true);
            }
        }
    }
}