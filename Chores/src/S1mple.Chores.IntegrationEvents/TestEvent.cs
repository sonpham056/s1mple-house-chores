﻿namespace S1mple.Chores.IntegrationEvents
{
    public interface TestEvent
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
